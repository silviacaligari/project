#include <cstdlib>
#include <iostream>
#include <cassert>
#include <algorithm>
#include <numeric>

#include "mars.hpp"

#include "prova_mass.hpp"
#include "mars_mesh_generation.hpp"

void test_assembly()
{
    using namespace mars;

    Mesh2 mesh;

    generate_square(mesh, 2, 2);

    FE<2,2> element;

    element.init_jacobian(mesh, 0);

    std::vector<Vector<Real, 2>> qp = {{0.,0.}, {1.,0.}, {0.,1.} };

    std::vector<Real> qw = {1./3, 1./3, 1./3};

    Matrix<Real,3, 3> mass; // 2+1

    MassMatrixAssembler<2,2> assembler;

    assembler.assemble(element, qp, qw, mass);

    mass.describe(std::cout);
}

int main(int argc, char *argv[])
{
    using namespace mars;
    
#ifdef WITH_MPI
    MPI_Init(&argc, &argv);
#endif //WITH_MPI
    test_assembly();
    
#ifdef WITH_MPI
    // par_mesh_test();
    return MPI_Finalize();
#else
    
    return 0;
#endif //WITH_MPI
}
