#include "mars_simplex.hpp" 
#include "mars_matrix.hpp"
#include "mars_vector.hpp"
#include "mars_static_math.hpp"

namespace mars {

   template <Integer Dim, Integer ManifoldDim>

   class FE  {
       public: 
       FE()
       : ref_measure(1./Factorial<ManifoldDim>::value)
       {}

       void init_fun(const Vector<Real,ManifoldDim> &x)
       {
           fun[0] = 1; 
           for (int i = 0; i<ManifoldDim; i++){
               fun[0] -= x[i];
               fun[i+1] = x[i];
           }

       }  

       void init_jacobian(const Mesh<Dim,ManifoldDim> &mesh, const Integer id_elem){
           const Simplex<Dim, ManifoldDim> &simplex = mesh.elem(id_elem);
           jacobian(simplex, mesh.points(), J); 
           det_J = det(J);
           measure = det_J*ref_measure;
       }


       Vector<Real, ManifoldDim+1> fun; 
       Matrix<Real, Dim, ManifoldDim> J; 
       Real det_J; 
       Real ref_measure; 
       Real measure;
   };

   template <Integer Dim, Integer ManifoldDim>

   class MassMatrixAssembler{
   public:
       
       void assemble(FE<Dim, ManifoldDim> &element,const std::vector<Vector<Real, ManifoldDim>> &qp, const std::vector<Real> &qw, Matrix<Real, ManifoldDim+1, ManifoldDim+1> &M )
       
       {
       int n=qp.size();
       M.zero(); //inizializzo la matrice a zero.
       
       for (int k=0; k<n; k++){
           
           element.init_fun(qp[k]); //stiamo inizializzando le funzioni di base (stiamo calcolando ogni funzione di base per un dato punto x_k). Lo facciamo
                                   // modificando lo stato di element, che diventa un vettore con componenti le funzioni di base.
           const Real qw_new = qw[k]*element.ref_measure; //creo nuovi pesi invece che modificare quelli che ho (anche perche ho dichiarato const i qw).
           
           for (int i=0; i<=ManifoldDim; i++) {
               for (int j=0; j<=ManifoldDim; j++) {
                   
                   M(i,j) = qw_new*element.fun[i]*element.fun[j]; //M(i,j) per come ho definito l'oggetto Matrix in Mars.
               
           }
       }
       }
     }
   };
}
